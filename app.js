var express = require('express')
var bodyParser= require('body-parser')
var http = require('http');
var debug = require('debug')('refill:server');
var database = require('./Config/connection')
var common = require('./Common/response')
var routes = require('./Router/index')
var cors = require('cors')
var app = express() 

app.use(cors())
var server = http.createServer(app)
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/',(req,res)=>{
  res.send("hello")
})

app.use('/api', (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) {
      res.json({
        message: "Authorization token is not found." 
      })
      return;
    }
    const app_token = common.getAuthoriztionToken()
    if (token === app_token) {
      next();
    } else
    res.json({
      message: "Authorization token is not matched.",
      token:app_token
    })
}, routes); 


module.exports = app
