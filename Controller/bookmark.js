var validate = require('../Common/common')
var httpResponse = require('../Common/response')
var Bookmark = require('../Modal/bookmark')
var { SUCCESS, ERROR, VALIDATE_ERROR, ERROR} = require('../Common/constant')

/** Create role Api */
var createBookmark = (req, res) => {
    const reqBody = {
        link: req.body.link,
        userId: req.body.userId,
        folderId:req.body.folderId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var obj = Object.assign({},reqBody,{ permission: req.body.permission})
                var bookmark = new Bookmark(obj)
                bookmark.save()
                .then(data => {
                    httpResponse.httpResponse(req,res,SUCCESS,data)
                }).catch(err => {
                    httpResponse.httpResponse(req,res,ERROR,err)
                }); 
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get role Api */
var getBookmark = (req, res) => {
    const reqBody = {
        folderId:req.body.folderId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Bookmark.aggregate([
                    { $match : {folderId: mongoose.Types.ObjectId(req.body.folderId)} },
                    {
                        $lookup:
                          {
                            from: "folders",
                            localField: "folderId",
                            foreignField: "_id",
                            as: "folderDetail"
                          }
                     },
                     {
                        $lookup:
                          {
                            from: "adminusers",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userDetail"
                          }
                     }
                ],function(err,data){
                    if(err){
                        httpResponse.httpResponse(req,res,ERROR,err)
                    }else{
                        httpResponse.httpResponse(req,res,SUCCESS,data)
                    }
                })
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get By Id role Api */
var getBookmarkById = (req, res) => {
    const reqBody = {
        _id:req.params.id
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Bookmark.find(reqBody)
                    .then((data)=>{
                        if(data && data.length > 0){
                            httpResponse.httpResponse(req,res,SUCCESS,data[0])
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message :"No role present"})
                        }
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}


/** Delete role Api */
var deleteBookmark = (req, res) => {
    const reqBody = {
        id:req.body.id
    }
    console.log(reqBody)
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Bookmark.deleteOne({_id:reqBody.id})
                    .then((data)=>{
                            httpResponse.httpResponse(req,res,SUCCESS,{message:"Bookmark delete successfully",result:data})
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Update role Api */
var updateBookmark = (req, res) => {
    const reqBody = {
        link: req.body.link,
        userId: req.body.userId,
        folderId:req.body.folderId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var obj = Object.assign({},reqBody,{ permission: req.body.permission})
               Bookmark.find({_id:req.body.id})
                    .then((data)=>{
                        if(data && data.length > 0){
                            Bookmark.updateOne({_id:req.body.id},{$set:obj}, async function(err,update){
                                if(err){
                                    httpResponse.httpResponse(req,res,NOVALUE,err) 
                                }else{
                                    const result = await Bookmark.find({_id:req.body.id})
                                    httpResponse.httpResponse(req,res,SUCCESS,result[0])
                                }
                            })
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message:"Role not found"})  
                        }
                    }).catch(err=>httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/**Get bookmark with userId */
var getBookmarkWithusetrId = (req, res) => {
    var array = []
    for(var i=0;i<req.body.bookmarkArray.length;i++){
        array.push(mongoose.Types.ObjectId(req.body.bookmarkArray[i]))
    }
    const reqBody = {
        bookmarkArray:req.body.bookmarkArray,
        userId:req.body.userId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Bookmark.aggregate([
                    { $match : 
                        {
                            $or : [
                                {
                                    userId: mongoose.Types.ObjectId(req.body.userId)
                                },
                                {
                                    _id : 
                                    { 
                                       $in : array
                                    }
                                } 
                            ]
                        },
                       
                    }
                ],function(err,data){
                    if(err){
                        httpResponse.httpResponse(req,res,ERROR,err)
                    }else{
                        httpResponse.httpResponse(req,res,SUCCESS,data)
                    }
                })
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

module.exports = {
    createBookmark,
    getBookmark,
    getBookmarkById,
    deleteBookmark,
    updateBookmark,
    getBookmarkWithusetrId
}