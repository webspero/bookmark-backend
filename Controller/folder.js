var validate = require('../Common/common')
var httpResponse = require('../Common/response')
var Folder = require('../Modal/folder')
var { SUCCESS, ERROR, VALIDATE_ERROR, ERROR} = require('../Common/constant')
const mongoose = require('mongoose');

/** Create role Api */
var createFolder = (req, res) => {
    const reqBody = {
        folderName: req.body.folderName,
        userId: req.body.userId,
        permission: req.body.permission
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var obj = Object.assign({},reqBody,{folderParentId:req.body.folderParentId})
                var folder = new Folder(obj)
                folder.save()
                .then(data => {
                    httpResponse.httpResponse(req,res,SUCCESS,data)
                }).catch(err => {
                    httpResponse.httpResponse(req,res,ERROR,err)
                }); 
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get role Api */
var getFolder = (req, res) => {
    const reqBody = {
        userId:req.body.userId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Folder.aggregate([
                    { $match : {userId: mongoose.Types.ObjectId(req.body.userId)} },
                    {
                        $lookup:
                          {
                            from: "adminusers",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userDetail"
                          }
                     },
                     {
                        $lookup:
                          {
                            from: "folders",
                            localField: "folderParentId",
                            foreignField: "_id",
                            as: "folderDetail"
                          }
                     }
                ],function(err,data){
                    if(err){
                        httpResponse.httpResponse(req,res,ERROR,err)
                    }else{
                        httpResponse.httpResponse(req,res,SUCCESS,data)
                    }
                })
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get By Id role Api */
var getFolderById = (req, res) => {
    const reqBody = {
        _id:req.params.id
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Folder.find(reqBody)
                    .then((data)=>{
                        if(data && data.length > 0){
                            httpResponse.httpResponse(req,res,SUCCESS,data[0])
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message :"No role present"})
                        }
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}


/** Delete role Api */
var deleteFolder = (req, res) => {
    const reqBody = {
        id:req.body.id
    }
    console.log(reqBody)
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Folder.deleteOne({_id:reqBody.id})
                    .then((data)=>{
                            httpResponse.httpResponse(req,res,SUCCESS,{message:"Folder delete successfully",result:data})
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Update role Api */
var updateFolder = (req, res) => {
    const reqBody = {
        folderName: req.body.folderName,
        userId: req.body.userId,
        permission: req.body.permission
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var obj = Object.assign({},reqBody,{folderParentId:req.body.folderParentId})
                Folder.find({_id:req.body.id})
                    .then((data)=>{
                        if(data && data.length > 0){
                            Folder.updateOne({_id:req.body.id},{$set:obj}, async function(err,update){
                                if(err){
                                    httpResponse.httpResponse(req,res,NOVALUE,err) 
                                }else{
                                    const result = await Folder.find({_id:req.body.id})
                                    httpResponse.httpResponse(req,res,SUCCESS,result[0])
                                }
                            })
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message:"Role not found"})  
                        }
                    }).catch(err=>httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/**Get folder with userId */
var getFolderWithusetrId = (req, res) => {
    var array = []
    for(var i=0;i<req.body.folderArray.length;i++){
        array.push(mongoose.Types.ObjectId(req.body.folderArray[i]))
    }
    const reqBody = {
        folderArray:req.body.folderArray,
        userId:req.body.userId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Folder.aggregate([
                    // { $match : {userId: mongoose.Types.ObjectId(req.body.userId)} }
                    { $match : 
                        {
                            $or : [
                                {userId: mongoose.Types.ObjectId(req.body.userId)},
                                {
                                    _id : 
                                    { 
                                       $in : array
                                    }
                                } 
                            ]
                        },
                       
                    }
                ],function(err,data){
                    if(err){
                        httpResponse.httpResponse(req,res,ERROR,err)
                    }else{
                        httpResponse.httpResponse(req,res,SUCCESS,data)
                    }
                })
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

module.exports = {
    createFolder,
    getFolder,
    getFolderById,
    deleteFolder,
    updateFolder,
    getFolderWithusetrId                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
}