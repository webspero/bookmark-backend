var validate = require('../Common/common')
var httpResponse = require('../Common/response')
var Assign = require('../Modal/assign')
var { SUCCESS, ERROR, VALIDATE_ERROR, ERROR} = require('../Common/constant')

/** Create Assign Api */
var createAssign = (req, res) => {
    const reqBody = {
        folderId: req.body.folderId,
        assignTo: req.body.assignTo,
        assignBy: req.body.assignBy
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var assign = new Assign(reqBody)
                assign.save()
                .then(data => {
                    httpResponse.httpResponse(req,res,SUCCESS,data)
                }).catch(err => {
                    httpResponse.httpResponse(req,res,ERROR,err)
                }); 
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get Assign Api */
var getAssign = (req, res) => {
    const reqBody = {
        assignTo:req.body.assignTo
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Assign.find(reqBody)
                    .then((data)=>{
                        if(data && data.length > 0){
                            httpResponse.httpResponse(req,res,SUCCESS,data)
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message :"No Assign present"})
                        }
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get By Id Assign Api */
var getAssignById = (req, res) => {
    const reqBody = {
        _id:req.params.id
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Assign.find(reqBody)
                    .then((data)=>{
                        if(data && data.length > 0){
                            httpResponse.httpResponse(req,res,SUCCESS,data[0])
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message :"No Assign present"})
                        }
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}


/** Delete Assign Api */
var deleteAssign = (req, res) => {
    const reqBody = {
        id:req.body.id
    }
    
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Assign.deleteOne({_id:reqBody.id})
                    .then((data)=>{
                            httpResponse.httpResponse(req,res,SUCCESS,{message:"Assign delete successfully",result:data})
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Update Assign Api */
var updateAssign = (req, res) => {
    const reqBody = {
        folderId: req.body.folderId,
        assignTo: req.body.assignTo,
        assignBy: req.body.assignBy
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
               Assign.find({_id:req.body.id})
                    .then((data)=>{
                        if(data && data.length > 0){
                            Assign.updateOne({_id:req.body.id},{$set:reqBody}, async function(err,update){
                                if(err){
                                    httpResponse.httpResponse(req,res,NOVALUE,err) 
                                }else{
                                    const result = await Assign.find({_id:req.body.id})
                                    httpResponse.httpResponse(req,res,SUCCESS,result[0])
                                }
                            })
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message:"Assign not found"})  
                        }
                    }).catch(err=>httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

module.exports = {
    createAssign,
    getAssign,
    getAssignById,
    deleteAssign,
    updateAssign
}