var validate = require('../Common/common')
var httpResponse = require('../Common/response')
var Role = require('../Modal/role')
var { SUCCESS, ERROR, VALIDATE_ERROR, ERROR} = require('../Common/constant')

/** Create role Api */
var createRole = (req, res) => {
    const reqBody = {
        roleName: req.body.roleName,
        userId: req.body.userId,
        permission: req.body.permission
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var role = new Role(reqBody)
                role.save()
                .then(data => {
                    httpResponse.httpResponse(req,res,SUCCESS,data)
                }).catch(err => {
                    httpResponse.httpResponse(req,res,ERROR,err)
                }); 
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get role Api */
var getRole = (req, res) => {
    const reqBody = {
        userId:req.body.userId
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Role.find(reqBody)
                    .then((data)=>{
                        if(data && data.length > 0){
                            httpResponse.httpResponse(req,res,SUCCESS,data)
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message :"No role present"})
                        }
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Get By Id role Api */
var getRoleById = (req, res) => {
    const reqBody = {
        _id:req.params.id
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Role.find(reqBody)
                    .then((data)=>{
                        if(data && data.length > 0){
                            httpResponse.httpResponse(req,res,SUCCESS,data[0])
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message :"No role present"})
                        }
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}


/** Delete role Api */
var deleteRole = (req, res) => {
    const reqBody = {
        id:req.body.id
    }
    console.log(reqBody)
    validate.validation(Object.keys(reqBody), reqBody)
        .then(async ({ status, response }) => {
            if (status) {
                Role.deleteOne({_id:reqBody.id})
                    .then((data)=>{
                            httpResponse.httpResponse(req,res,SUCCESS,{message:"Role delete successfully",result:data})
                    }).catch(err=> httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

/** Update role Api */
var updateRole = (req, res) => {
    const reqBody = {
        roleName: req.body.roleName,
        userId: req.body.userId,
        permission: req.body.permission
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
               Role.find({_id:req.body.id})
                    .then((data)=>{
                        if(data && data.length > 0){
                            Role.updateOne({_id:req.body.id},{$set:reqBody}, async function(err,update){
                                if(err){
                                    httpResponse.httpResponse(req,res,NOVALUE,err) 
                                }else{
                                    const result = await Role.find({_id:req.body.id})
                                    httpResponse.httpResponse(req,res,SUCCESS,result[0])
                                }
                            })
                        }else{
                            httpResponse.httpResponse(req,res,ERROR,{message:"Role not found"})  
                        }
                    }).catch(err=>httpResponse.httpResponse(req,res,ERROR,err))
            } else {
                httpResponse.httpResponse(req,res,VALIDATE_ERROR,response)
            }
        }).catch(err => httpResponse.httpResponse(req,res,VALIDATE_ERROR,err));
}

module.exports = {
    createRole,
    getRole,
    getRoleById,
    deleteRole,
    updateRole
}