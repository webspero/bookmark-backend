var validate = require('../Common/common')
var httpResponse = require('../Common/response')
var User = require('../Modal/user')
var AdminUser = require('../Modal/adminUser')
var jwt = require('jsonwebtoken');
var { SUCCESS, ERROR, VALIDATE_ERROR, NOT_VALID, NOVALUE, ERROR } = require('../Common/constant')

/*Normal user Signup Api */
var NormalUserSignup = (req, res) => { 
    const reqBody = {
        userName: req.body.userName,
        email: req.body.email,
        userType: req.body.userType,
        roleId: req.body.roleId,
        password: httpResponse.encryption(req.body.password),
        confirm_password: httpResponse.encryption(req.body.confirm_password)
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var obj = Object.assign({}, reqBody, { parentId: req.body.parentId })
                const user = new User(obj);
                User.find({ email: req.body.email }).then((data) => {
                    if (data && data.length === 0) {
                        user.save()
                            .then(data => {
                                httpResponse.httpResponse(req, res, SUCCESS, data)
                            }).catch(err => {
                                httpResponse.httpResponse(req, res, ERROR, err)
                            });
                    } else {
                        httpResponse.httpResponse(req, res, NOVALUE, { message: "Email already present" })
                    }
                }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))

            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/** Admin user Signup Api */
var AdminUserSignup = (req, res) => {
    const reqBody = {
        userName: req.body.userName,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        userType: req.body.userType,
        companyName: req.body.companyName,
        phoneNo: req.body.phoneNo,
        password: httpResponse.encryption(req.body.password),
        confirm_password: httpResponse.encryption(req.body.confirm_password)
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                const Adminuser = new AdminUser(reqBody);
                AdminUser.find({ email: req.body.email }).then((data) => {
                    if (data && data.length === 0) {
                        Adminuser.save()
                            .then(data => {
                                httpResponse.httpResponse(req, res, SUCCESS, data)
                            }).catch(err => {
                                httpResponse.httpResponse(req, res, ERROR, err)
                            });
                    } else {
                        httpResponse.httpResponse(req, res, NOVALUE, { message: "Email already present" })
                    }
                }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))

            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

var signup = (req, res) => {
    if (req.body.userType === 'ADMIN') {
        AdminUserSignup(req, res)
    } else if (req.body.userType === 'USER') {
        NormalUserSignup(req, res)
    } else if (req.body.userType === '') {
        httpResponse.httpResponse(req, res, NOVALUE, { message: "User type required" })
    } else {
        httpResponse.httpResponse(req, res, NOVALUE, { message: "User type not exists" })
    }
}

/* Login Api */
var login = (req, res) => {
    const reqBody = {
        email: req.body.email,
        password: httpResponse.encryption(req.body.password),
        userType: req.body.userType
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                if (reqBody.userType === 'ADMIN') {
                    AdminUser.find(reqBody).then((data) => {
                        if (data && data.length === 0) {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "Email or password either wrong" })
                        } else {
                            httpResponse.httpResponse(req, res, SUCCESS, data[0])
                        }
                    }).catch((err) => {
                        httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                    })
                } else if (reqBody.userType === 'USER') {
                    User.find(reqBody).then((data) => {
                        if (data && data.length === 0) {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "Email or password either wrong" })
                        } else {
                            httpResponse.httpResponse(req, res, SUCCESS, data[0])
                        }
                    }).catch((err) => {
                        httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                    })
                } else {
                    httpResponse.httpResponse(req, res, NOVALUE, { message: "User type not exists" })
                }

            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/* SessionLogin Api */
var sessionlogin = (req, res) => {
    const reqBody = {
        usertoken: req.body.usertoken
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                jwt.verify(reqBody.usertoken, 'shhhhh', function (err, decoded) {
                    if (err) {
                        httpResponse.httpResponse(req, res, NOT_VALID, { message: "User Token expire" })
                    } else {
                        User.find({ _id: decoded.id }).then((data) => {
                            jwt.sign({ id: data[0]._id }, 'shhhhh', { expiresIn: 60 }, (err, token) => {
                                if (err) {
                                    httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { ...data[0]._doc, usertoken: token })
                                }
                            })
                        }).catch((err) => {
                            httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                        })
                    }
                });
            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => {
            httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
        });
}

/* ResetPassword Api */
var resetPassword = (req, res) => {
    const reqBody = {
        email: req.body.email,
        userType: req.body.userType,
        password: httpResponse.encryption(req.body.password),
        confirm_password: httpResponse.encryption(req.body.confirm_password)
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                if (reqBody.userType === 'ADMIN') {
                    AdminUser.find({ email: req.body.email }).then((data) => {
                        if (data && data.length) {
                            AdminUser.updateOne({ _id: data[0]._id }, { $set: reqBody }, function (err, data) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { message: "Password reset successfully" })
                                }
                            })
                        } else {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "User Not Present" })
                        }
                    }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))
                } else if (reqBody.userType === 'USER') {
                    User.find({ email: req.body.email }).then((data) => {
                        if (data && data.length) {
                            User.updateOne({ _id: data[0]._id }, { $set: reqBody }, function (err, data) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { message: "Password reset successfully" })
                                }
                            })
                        } else {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "User Not Present" })
                        }
                    }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))
                } else {
                    httpResponse.httpResponse(req, res, NOVALUE, { message: "User type not exists" })
                }
            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/* ForgotPassword Api */
var forgotPassword = (req, res) => {
    const reqBody = {
        email: req.body.email,
        userType: req.body.userType
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                if (reqBody.userType === 'ADMIN') {
                    AdminUser.find({ email: reqBody.email }).then((data) => {
                        if (data && data.length) {
                            const chnagedPassword = httpResponse.ramdomPassword(parseInt(Math.random() * 1000000))
                            AdminUser.updateOne({ _id: data[0]._id }, { $set: { password: chnagedPassword.encrypted, confirm_password: chnagedPassword.encrypted } }, function (err, data) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, NOVALUE, data)
                                } else {
                                    httpResponse.mail(reqBody.email, chnagedPassword.ramdomPassword, (status, data) => {
                                        if (status) {
                                            httpResponse.httpResponse(req, res, SUCCESS, { message: "Password send your email" })
                                        } else {
                                            httpResponse.httpResponse(req, res, NOVALUE, data)
                                        }
                                    })
                                }
                            })

                        } else {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "Email not present" })
                        }
                    }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))
                } else if (reqBody.userType === 'USER') {
                    User.find({ email: reqBody.email }).then((data) => {
                        if (data && data.length) {
                            const chnagedPassword = httpResponse.ramdomPassword(parseInt(Math.random() * 1000000))
                            User.updateOne({ _id: data[0]._id }, { $set: { password: chnagedPassword.encrypted, confirm_password: chnagedPassword.encrypted } }, function (err, data) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, NOVALUE, data)
                                } else {
                                    httpResponse.mail(reqBody.email, chnagedPassword.ramdomPassword, (status, data) => {
                                        if (status) {
                                            httpResponse.httpResponse(req, res, SUCCESS, { message: "Password send your email" })
                                        } else {
                                            httpResponse.httpResponse(req, res, NOVALUE, data)
                                        }
                                    })
                                }
                            })

                        } else {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "Email not present" })
                        }
                    }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))
                } else {
                    httpResponse.httpResponse(req, res, NOVALUE, { message: "User type not exists" })
                }
            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/* ChangePassword Api */
var changePassword = (req, res) => {
    const reqBody = {
        oldPassword: httpResponse.encryption(req.body.oldPassword),
        userType: req.body.userType,
        password: httpResponse.encryption(req.body.password),
        confirm_password: httpResponse.encryption(req.body.confirm_password)
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                if (reqBody.userType === 'ADMIN') {
                    AdminUser.find({ _id: req.body.id }).then((data) => {
                        if (data && data[0].password === reqBody.oldPassword) {
                            AdminUser.updateOne({ _id: data[0]._id }, { $set: reqBody }, function (err, data) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { message: "Password changed successfully" })
                                }
                            })
                        } else {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "Old password wrong" })
                        }
                    }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))
                } else if (reqBody.userType === 'USER') {
                    User.find({ _id: req.body.id }).then((data) => {
                        if (data && data[0].password === reqBody.oldPassword) {
                            User.updateOne({ _id: data[0]._id }, { $set: reqBody }, function (err, data) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { message: "Password Changed successfully" })
                                }
                            })
                        } else {
                            httpResponse.httpResponse(req, res, NOVALUE, { message: "Old Password wrong" })
                        }
                    }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))
                } else {
                    httpResponse.httpResponse(req, res, NOVALUE, { message: "User type not exists" })
                }
            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/* Updateuser Api */
var updateUser = (req, res) => {
    if (req.body.userType === 'ADMIN') {
        AdminUserUpdate(req, res)
    } else if (req.body.userType === 'USER') {
        NormalUserUpdate(req, res)
    } else if (req.body.userType === '') {
        httpResponse.httpResponse(req, res, NOVALUE, { message: "User type required" })
    } else {
        httpResponse.httpResponse(req, res, NOVALUE, { message: "User type not exists" })
    }
}

/*Normal user update Api */
var NormalUserUpdate = (req, res) => {
    const reqBody = {
        userName: req.body.userName,
        email: req.body.email,
        userType: req.body.userType,
        roleId: req.body.roleId,
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                var obj = Object.assign({}, reqBody, { parentId: req.body.parentId })
                User.find({ _id: req.body.id }).then((data) => {
                    if (data && data.length !== 0) {
                        if (data[0].email === reqBody.email) {
                            User.updateOne({ _id: req.body.id }, { $set: obj }, async function (err, update) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, NOVALUE, err)
                                } else {
                                    const result = await User.find({ _id: req.body.id })
                                    httpResponse.httpResponse(req, res, SUCCESS, result[0])
                                }
                            })
                        } else {
                            User.find({ email: reqBody.email }).then((result) => {
                                console.log(result)
                                if (result && result.length === 0) {
                                    User.updateOne({ _id: req.body.id }, { $set: obj }, async function (err, update) {
                                        if (err) {
                                            httpResponse.httpResponse(req, res, NOVALUE, err)
                                        } else {
                                            const result = await User.find({ _id: req.body.id })
                                            httpResponse.httpResponse(req, res, SUCCESS, result[0])
                                        }
                                    })
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { message: "Email already present" })
                                }
                            }).catch(err => httpResponse.httpResponse(req, res, ERROR, err))
                        }

                    } else {
                        httpResponse.httpResponse(req, res, NOVALUE, { message: "User Not present" })
                    }
                }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))

            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/** Admin user Signup Api */
var AdminUserUpdate = (req, res) => {
    const reqBody = {
        userName: req.body.userName,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        userType: req.body.userType,
        companyName: req.body.companyName,
        phoneNo: req.body.phoneNo,
    }
    validate.validation(Object.keys(reqBody), reqBody)
        .then(({ status, response }) => {
            if (status) {
                AdminUser.find({ _id: req.body.id }).then((data) => {
                    if (data && data.length !== 0) {
                        if (data[0].email === reqBody.email) {
                            AdminUser.updateOne({ _id: req.body.id }, { $set: reqBody }, async function (err, update) {
                                if (err) {
                                    httpResponse.httpResponse(req, res, NOVALUE, err)
                                } else {
                                    const result = await AdminUser.find({ _id: req.body.id })
                                    httpResponse.httpResponse(req, res, SUCCESS, result[0])
                                }
                            })
                        } else {
                            AdminUser.find({ email: reqBody.email }).then((results) => {
                                if (results && results.length === 0) {
                                    AdminUser.updateOne({ _id: req.body.id }, { $set: reqBody }, async function (err, update) {
                                        if (err) {
                                            httpResponse.httpResponse(req, res, NOVALUE, err)
                                        } else {
                                            const result = await AdminUser.find({ _id: req.body.id })
                                            httpResponse.httpResponse(req, res, SUCCESS, result[0])
                                        }
                                    })
                                } else {
                                    httpResponse.httpResponse(req, res, SUCCESS, { message: "Email already present" })
                                }
                            }).catch(err => httpResponse.httpResponse(req, res, ERROR, err))
                        }

                    } else {
                        httpResponse.httpResponse(req, res, NOVALUE, { message: "User Not present" })
                    }
                }).catch((err) => httpResponse.httpResponse(req, res, ERROR, err))

            } else {
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
            }
        }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}

/** Get user data */
var getUser = (req, res) => {
    let reqBody
    if(req.body.parentId){
        reqBody = {
            parentId:req.body.parentId,
            roleId:req.body.roleId
        }
    }else{
        reqBody = {
            roleId:req.body.roleId
        }
    }
    validate.validation(Object.keys({roleId:req.body.roleId}), {roleId:req.body.roleId})
    .then(({ status, response }) => {
        if (status) {
            User.find(reqBody).then((result)=>{
                httpResponse.httpResponse(req, res, SUCCESS, result)
            }).catch((err)=>{
                httpResponse.httpResponse(req, res, VALIDATE_ERROR, err)
            })
        } else {
            httpResponse.httpResponse(req, res, VALIDATE_ERROR, response)
        }
    }).catch(err => httpResponse.httpResponse(req, res, VALIDATE_ERROR, err));
}


module.exports = {
    signup,
    login,
    getUser,
    // sessionlogin,
    resetPassword,
    forgotPassword,
    changePassword,
    updateUser
}