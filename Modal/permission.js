const mongoose = require('mongoose');

const permissionSchema = mongoose.Schema({
    create:{
        type:Boolean,
        default:false,
        required:true
    },
    read:{
        type:Boolean,
        default:false,
        required:true
    },
    view:{
        type:Boolean,
        default:false,
        required:true
    },
    delete:{
        type:Boolean,
        default:false,
        required:true
    }
})

module.exports = mongoose.model('permission',permissionSchema)