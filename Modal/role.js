const mongoose = require('mongoose');
var permission = require('./permission').schema
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const roleSchema = mongoose.Schema({
    roleName:{
        type:String,
        required:true
    },
    permission:[permission],
    userId:{
        type:ObjectId,
        required:true
    }
}, {
    timestamps: true
})
module.exports = mongoose.model('roles',roleSchema)
