const mongoose = require('mongoose');
var permission = require('./permission').schema
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const bookmarkSchema = mongoose.Schema({
    link:{
        type:String,
        required:true
    },
    userId:{
        type:ObjectId,
        required:true
    },
    folderId:{
        type:ObjectId,
        required:true
    },
    permission:[permission]
}, {
    timestamps: true
})
module.exports = mongoose.model('bookmark',bookmarkSchema)
