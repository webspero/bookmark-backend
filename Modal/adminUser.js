const mongoose = require('mongoose');

const adminuserSchema = mongoose.Schema({
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    confirm_password:{
        type:String,
        required:true
    },
    userName:{
        type:String,
        required:true
    },
    firstName:{
        type:String,
        required:true
    },
    lastName:{
        type:String,
        required:true
    },
    userType:{
        type:String,
        required:true
    },
    phoneNo:{
        type:String,
        required:true
    },
    companyName:{
        type:String,
        required:true
    },
}, {
    timestamps: true
})
module.exports = mongoose.model('AdminUsers',adminuserSchema)
