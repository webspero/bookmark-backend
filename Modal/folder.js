const mongoose = require('mongoose');
var permission = require('./permission').schema
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const folderSchema = mongoose.Schema({
    folderName:{
        type:String,
        required:true
    },
    userId:{
        type:ObjectId,
        required:true
    },
    folderParentId:{
        type:ObjectId,
        required:false
    },
    permission:[permission]
}, {
    timestamps: true
})
module.exports = mongoose.model('folder',folderSchema)
