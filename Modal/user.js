const mongoose = require('mongoose');

const normalUserSchema = mongoose.Schema({
    userName:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    confirm_password:{
        type:String,
        required:true
    },
    userType:{
        type:String,
        required:true
    },
    roleId:{
        type:String,
        required:true
    },
    parentId:{
        type:String,
        required:false
    },
}, {
    timestamps: true
})
module.exports = mongoose.model('NormalUsers',normalUserSchema)
