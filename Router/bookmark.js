var express = require('express');
var router = express.Router();
var bookmark = require('../Controller/bookmark')

router.post('/create',bookmark.createBookmark);
router.post('/get',bookmark.getBookmark);
router.get('/get/:id',bookmark.getBookmarkById);
router.post('/delete',bookmark.deleteBookmark);
router.post('/update',bookmark.updateBookmark);
router.post('/getall',bookmark.getBookmarkWithusetrId);

module.exports = router