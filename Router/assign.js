var express = require('express');
var router = express.Router();
var assign = require('../Controller/assign')

router.post('/create',assign.createAssign);
router.post('/get',assign.getAssign);
router.get('/get/:id',assign.getAssignById);
router.post('/delete',assign.deleteAssign);
router.post('/update',assign.updateAssign);

module.exports = router