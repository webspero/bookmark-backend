var express = require('express');
var router = express.Router();
var user = require('./user')
var role = require('./role')
var folder = require('./folder')
var bookmark = require('./bookmark')
var assign = require('./assign')

router.use('/user',user);
router.use('/role',role);
router.use('/folder',folder);
router.use('/bookmark',bookmark);
router.use('/assign',assign)

module.exports = router