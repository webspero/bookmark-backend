var express = require('express');
var router = express.Router();
var role = require('../Controller/role')

router.post('/create',role.createRole);
router.post('/get',role.getRole);
router.get('/get/:id',role.getRoleById);
router.post('/delete',role.deleteRole);
router.post('/update',role.updateRole);

module.exports = router