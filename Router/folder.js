var express = require('express');
var router = express.Router();
var folder = require('../Controller/folder')

router.post('/create',folder.createFolder);
router.post('/get',folder.getFolder);
router.get('/get/:id',folder.getFolderById);
router.post('/delete',folder.deleteFolder);
router.post('/update',folder.updateFolder);
router.post('/getall',folder.getFolderWithusetrId)

module.exports = router